<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link href="<?php echo $basePath; ?>files/css/bootstrap.css" rel="stylesheet">
    <link href="<?php echo $basePath; ?>files/css/style.css" rel="stylesheet">

    <script src="<?php echo $basePath; ?>files/js/jquery-3.5.1.min.js"></script>

    <title>Advertising Systems Loader</title>
</head>
<body>
<nav class="navbar navbar-expand-md navbar-dark bg-dark fixed-top">
    <a class="navbar-brand" href="#">Advertising Systems Loader</a>
</nav>

<main class="container content">
    <?php include "homepage.php" ?>
</main>
</body>
</html>
