<div class="alert alert-success" role="alert">
    Import do DB se povedl!
</div>

<div class="alert alert-danger" role="alert">
    Během importu došlo k chybě!
</div>

<div id="banner">
    <h1 class="text-center">AdSystemsLoader</h1>
</div>

<div id="content">
    <div class="container mt-1">
        <div class="form-group">
            <button name="input" id="loadData" class="btn-dark form-control">Načíst reklamní informace do DB</button>
        </div>
    </div>
</div>
<script>
    $(".alert.alert-danger").hide();
    $(".alert.alert-success").hide();

    $('#loadData').on('click', function () {
        $.ajax({
            url: '/adsystemcontroller/actionLoadAdData',
            success: function (data) {
                if (data === true)
                    $(".alert.alert-success").show();
                else
                    $(".alert.alert-danger").show();
            }
            // TODO add error catch
        });
    });
</script>