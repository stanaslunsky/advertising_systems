<?php

class BaseController
{
    protected string $basePath;
    protected mysqli $databaseConnection;

    /**
     * BaseController constructor.
     */
    public function __construct() {
        $this->basePath = '/adsystems/';
        $this->databaseConnection = $this->createDatabaseConnection();
    }

    /**
     * Run action.
     *
     * @param string $action
     * @return mixed
     */
    public function run(string $action)
    {
        if (!method_exists($this, $action)) {
            $action = 'showHomepage';
        }

        return $this->$action();
    }

    /**
     * Load default page layout.
     */
    protected function showHomepage()
    {
        $basePath = $this->basePath;

        include 'views/layout.php';
    }

    /**
     * Creates DB mysqli connection.
     *
     * @return mysqli DB connection
     */
    private function createDatabaseConnection(): mysqli {
        $mysqli = new mysqli("localhost", "user", "password", "database");

        if ($mysqli->connect_errno) {
            // TODO log error
            exit();
        }

        return $mysqli;
    }

}