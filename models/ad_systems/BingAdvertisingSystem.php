<?php


class BingAdvertisingSystem extends BaseAdvertisingSystem
{
    /**
     * BingAdvertisingSystem Constructor
     */
    public function __constructor()
    {
        $this->fileName = 'files/import.csv';
        $this->advertisingSystemId = 4;
    }

    /**
     * Load file and returns extracted data.
     *
     * @return array Data
     */
    public function loadFile(): array
    {
        return Convertor::getArrayFromXml($this->fileName);
    }

    /**
     * Format data and array into required.
     *
     * @param array $data
     * @return array
     */
    public function getFormattedData(array $data): array {
        // TODO get needed data, format into base array which can be processed with foreach
        return $data;
    }
}