<?php


abstract class BaseAdvertisingSystem
{
    protected int $advertisingSystemId;
    protected string $fileName;

    /**
     * Main method, processing content from file to array to AdSystemModel and then inserts into DB.
     *
     * @return bool
     */
    public function getProcessedAdData(): bool
    {
        $fileContent = $this->loadFile();
        $advertisingModels = $this->getProcessedData($fileContent);
        $queryResult = $this->insertAdDataIntoDb($advertisingModels);

        return $queryResult;
    }

    /**
     * Creates and returns AdSystemModels with parsed required attributes.
     *
     * @param array $data Data in required format
     * @return array Array of AdSystemModels
     */
    protected function getProcessedData(array $data): array
    {
        $formattedData = $this->getFormattedData($data);

        $adSystemModels = [];

        foreach ($formattedData as $key => $values) {
            $adSystemModel = new AdvertisingSystemModel();
            $adSystemModel->setCampaign($values['campaign']);
            $adSystemModel->setDatetime($values['datetime']);
            $adSystemModel->setImpressionsNumber($values['impressionsNumber']);
            $adSystemModel->setClicksNumber($values['clicksNumber']);
            $adSystemModel->setConversionsNumber($values['conversionsNumber']);
            $adSystemModel->setPrice($values['price']);
            $adSystemModel->setKeywords($values['keywords']);
        }

        return $adSystemModels;
    }


    /**
     * Base method for database inserts.
     *
     * @param array $advertisingSystemModels
     * @return bool
     */
    protected function insertAdDataIntoDb(array $advertisingSystemModels): bool
    {
        foreach ($advertisingSystemModels as $advertisingSystemModel) {
            // TODO In methods do:
            // TODO insert campaign into table ad_campaign if not exists
            // TODO insert group into table ad_group if not exists
            // TODO insert keywords into table ad_keyword and ad_group_keyword if not exists
            // TODO insert ad system data into table ad_system_data
        }

        return false;
    }

    abstract function loadFile(): array;

    abstract function getFormattedData(array $data): array;
}