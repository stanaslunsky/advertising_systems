<?php


class SklikAdvertisingSystem extends BaseAdvertisingSystem
{
    /**
     * SklikAdvertisingSystem Constructor
     */
    public function __constructor()
    {
        $this->fileName = 'files/import_sklik.json';
        $this->advertisingSystemId = 3;
    }

    /**
     * Load file and returns extracted data.
     *
     * @return array Data
     */
    public function loadFile(): array
    {
        return Convertor::getArrayFromJson($this->fileName);
    }

    /**
     * Format data and array into required.
     *
     * @param array $data
     * @return array
     */
    public function getFormattedData(array $data): array {
        // TODO get needed data, format into base array which can be processed with foreach
        return $data;
    }
}