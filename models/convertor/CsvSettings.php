<?php

/**
 * Class CsvSettings
 */
class CsvSettings
{
    private array $settings;

    /**
     * @param string $delimiter Delimiter
     * @param string $enclosure Enclosure
     * @param int $length Length
     */
    public function __constructor(string $delimiter, string $enclosure, int $length) {
        $this->settings['delimiter'] = $delimiter;
        $this->settings['enclosure'] = $enclosure;
        $this->settings['length'] = $length;
    }

    /**
     * Return CSV settings.
     *
     * @return array CSV settings
     */
    public function getDelimiter(): array {
        return $this->settings;
    }

}